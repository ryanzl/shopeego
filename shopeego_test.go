package shopeego

import (
	"fmt"
	"testing"
)

func TestShopeeClient_CancelAuthPartner(t *testing.T) {
	// 初始化一個 Shopee 客戶端。
	client := NewClient(&ClientOptions{
		Secret: "a356da03376efcad0c88d440788aa4c1ed33bedb226edf7e5e4e975a85993437",
		IsSandbox: false,
	})
	partnerId := 2000578
	ret := client.CancelAuthPartner(&CancelAuthPartnerRequest{
		PartnerID: int64(partnerId),
		Redirect: "http://ynxs.devtech.top/shopee/authCb",
	})
	fmt.Println(ret) // 輸出：yamiodymel
}